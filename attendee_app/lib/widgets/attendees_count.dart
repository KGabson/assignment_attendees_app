import 'package:flutter/material.dart';
import 'package:attendee_app/logic/attendees_logic.dart';

class AttendeeCountWidget extends StatelessWidget {
  const AttendeeCountWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Map<String, dynamic>>>(
      stream: AttendeesLogic.attendeesStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final meetingsData = snapshot.data!;
          return ListView.builder(
            itemCount: meetingsData.length,
            itemBuilder: (context, index) {
              final meeting = meetingsData[index];
              final meetingId = meeting['meetingId'];
              final attendees = meeting['attendees'];

              return Card(
                elevation: 2,
                margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                child: ListTile(
                  title: Text('Attendees: $attendees'),
                  subtitle: Text('Meeting ID: $meetingId'),
                ),
              );
            },
          );
        }
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}