import 'package:attendee_app/widgets/attendees_count.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Meeting Attendees'),
      ),
      body: const Center(
        child: AttendeeCountWidget(),
      ),
    );
  }
}