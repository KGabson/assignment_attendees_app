import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class AttendeesLogic {
  static final StreamController<List<Map<String, dynamic>>> _attendeesStreamController =
      StreamController<List<Map<String, dynamic>>>();

  static Stream<List<Map<String, dynamic>>> get attendeesStream =>
      _attendeesStreamController.stream;

  static void fetchAttendees() {
    Timer.periodic(const Duration(seconds: 1), (timer) async {
      try {
        final response = await http.get(Uri.parse('https://appattendeesws.oa.r.appspot.com/meeting/list'));
        if (response.statusCode == 200) {
          final data = json.decode(response.body);
          _attendeesStreamController.add(List.from(data));
        }
      } catch (error) {
        print("Error fetching attendees: $error");
      }
    });
  }

  static void dispose() {
    _attendeesStreamController.close();
  }
}