import 'package:attendee_app/logic/attendees_logic.dart';
import 'package:flutter/material.dart';
import 'package:attendee_app/app.dart';

void main() {
  runApp(const MyApp());
  AttendeesLogic.fetchAttendees();
}