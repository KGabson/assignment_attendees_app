M.AutoInit();

const meetingList = document.getElementById("meetingList");
const addMeetingBtn = document.querySelector('.addMeetingBtn');
const maxRows = 10;

// Fetch and display the list of meetings
function fetchAndDisplayMeetings() {
    fetch("https://appattendeesws.oa.r.appspot.com/meeting/list")
        .then(response => response.json())
        .then(data => {
            meetingList.innerHTML = '';

            data.forEach(meeting => {
                const row = createMeetingRow(meeting.meetingId, meeting.attendees);
                meetingList.appendChild(row);
            });
        })
        .catch(error => console.log('error', error));
}

// Create a row for a meeting
function createMeetingRow(meetingId, attendees) {
    const row = document.createElement("tr");
    row.innerHTML = `
        <td>${meetingId}</td>
        <td>
            <input type="number" value="${attendees}" class="attendeesInput">
        </td>
        <td>
            <button class="btn waves-effect waves-light updateBtn">Update</button>
        </td>
    `;

    const updateButton = row.querySelector('.updateBtn');
    updateButton.addEventListener('click', () => updateAttendees(meetingId, row));

    return row;
}

// Update attendees for a meeting
function updateAttendees(meetingId, row) {
    const attendeesInput = row.querySelector('.attendeesInput');
    const newAttendees = parseInt(attendeesInput.value, 10);

    fetch(`https://appattendeesws.oa.r.appspot.com/update/${meetingId}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            attendees: newAttendees
        })
    })
    .then(response => response.text())
    .then(result => {
        M.toast({html: 'Attendees updated successfully', classes: 'green darken-1'});
        fetchAndDisplayMeetings();
    })
    .catch(error => {
        M.toast({html: 'Failed to update attendees', classes: 'red darken-1'});
        console.log('error', error);
    });
}

// Add event listener for adding a new meeting
addMeetingBtn.addEventListener('click', () => {
    if (meetingList.children.length < maxRows) {
        const randomMeetingId = Math.random().toString(36).substr(2, 10);
        const newRow = createMeetingRow(randomMeetingId, 0);
        meetingList.appendChild(newRow);
    } else {
        M.toast({html: 'Maximum number of meetings reached', classes: 'red darken-1'});
    }
});

// Initial fetch and display
fetchAndDisplayMeetings();
