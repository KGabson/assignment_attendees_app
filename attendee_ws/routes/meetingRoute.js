const express = require('express');
const router = express.Router();
const { getMeetingList } = require('../controllers/meetingController');

router.get('/list', getMeetingList);

module.exports = router;