const { db } = require('../firebaseSetup');

exports.updateAttendees = async (req, res) => {
  try {
    const meetingId = req.params.meetingId;
    const newAttendeesCount = req.body.attendees;

    const doc = db.collection('meetings').doc(meetingId);
    await doc.set({ attendees: newAttendeesCount });
    res.status(200).send('Attendees updated');
  } catch (err) {
    console.error('Error update', err);
    res.status(500).send();
  }
};