const express = require('express');
const router = express.Router();
const { updateAttendees } = require('../controllers/updateController');

router.post('/:meetingId', updateAttendees);

module.exports = router;