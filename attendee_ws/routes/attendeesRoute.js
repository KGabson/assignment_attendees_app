const express = require('express');
const router = express.Router();
const { getAttendees } = require('../controllers/attendeesController');

router.get('/:meetingId', getAttendees);

module.exports = router;