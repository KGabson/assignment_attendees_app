const { db } = require('../firebaseSetup');

exports.getAttendees = async (req, res) => {
  try {
    const meetingId = req.params.meetingId;
    const snapshot = db.collection('meetings').doc(meetingId);
    const doc = await snapshot.get();

    if (!doc.exists) {
      console.log('Document does not exist');
      res.status(404).send('Not Found');
    } else {
      res.status(200).json(doc.data().attendees);
    }
  } catch (err) {
    console.error('Error fetch attendees ', err);
    res.status(500).send();
  }
};