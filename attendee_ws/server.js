const express = require('express');
const cors = require('cors');
const path = require('path');
const config = require('./config');

// Initialize Express app
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

// Import and use routes
const attendeesRoute = require('./routes/attendeesRoute');
const updateRoute = require('./routes/updateRoute');
const meetingRoute = require('./routes/meetingRoute');

app.use('/attendees', attendeesRoute);
app.use('/update', updateRoute);
app.use('/meeting', meetingRoute);

// Serve main interface
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/interface.html'));
});

// Start the server
app.listen(config.port, () => {
  console.log(`App listening on port ${config.port}`);
});