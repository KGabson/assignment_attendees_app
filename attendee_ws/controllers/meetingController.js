const { db } = require('../firebaseSetup');

exports.getMeetingList = async (req, res) => {
  try {
    const meetingsCollection = db.collection('meetings');
    const querySnapshot = await meetingsCollection.get();

    const meetingList = [];
    querySnapshot.forEach((doc) => {
      const meetingId = doc.id;
      const attendees = doc.data().attendees;
      meetingList.push({ meetingId, attendees });
    });

    res.status(200).json(meetingList);
  } catch (error) {
    console.error('Error fetch meetings', error);
    res.status(500).send();
  }
};